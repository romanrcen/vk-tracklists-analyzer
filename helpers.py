from tqdm import tqdm


def progress_bar(item):
    return tqdm(
        iterable=item,
        desc='Progress',
        leave=True,
        ncols=100,
        colour='green',
        mininterval=0.05,
        maxinterval=10,
        bar_format=(
            '{desc}: {percentage:.0f}% |{bar}| {n}/{total} [{elapsed}]\n'
        )
    )
