import csv
import os
import pandas

from constants import USERS_DATA_FILE_NAME


def export_tracklist_to_csv(user_data: dict, tracklist: list, file_name: str):
    if tracklist:
        field_names = ['owner_id', 'sex', 'city', 'track_id', 'artist', 'title']
        os.makedirs('tmp', exist_ok=True)
        file_path = os.path.join('tmp', f'{file_name}_tracklist.csv')

        # Open a file and preparing for writing:
        with open(file_path, mode='a', encoding='utf-8', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=field_names)
            writer.writeheader()

            for item in tracklist:
                writer.writerow(
                    {
                        'owner_id': item.get('owner_id'),
                        'sex': user_data.get('sex'),
                        'city': user_data.get('city'),
                        'track_id': item.get('id'),
                        'artist': item.get('artist'),
                        'title': item.get('title')
                    }
                )


def export_users_data_to_csv(
        users_data: dict, export_file_name: str = USERS_DATA_FILE_NAME
):
    # Open a file for writing:
    with open(export_file_name, mode='a', encoding='utf-8', newline='') as csvfile:

        # Preparation for export:
        field_names = ['id', 'city', 'sex']

        # Writer creation:
        writer = csv.DictWriter(csvfile, fieldnames=field_names)
        writer.writeheader()

        # Collecting user_info to file
        for user in users_data:
            writer.writerow({
                    'id': user,
                    'sex': users_data[user]['sex'],
                    'city': users_data[user]['city'],
                })

    print(f'Users info are placed in a file: {export_file_name}')


def import_users_data_from_csv(file_name: str = USERS_DATA_FILE_NAME) -> dict:
    print(f'Collecting user data from file: {file_name}')
    users_data = {}

    # Open a file for reading:
    with open(file_name, encoding='utf-8', newline='') as csvfile:
        # Reader creation:
        fields_names = ['id', 'city', 'sex']
        reader = csv.DictReader(csvfile, fieldnames=fields_names)

        for row in reader:
            users_data[str(row['id'])] = {
                'city': row['city'],
                'sex': row['sex']
            }

    return users_data


def merge_csv_files(
        tmp_directory: str,
        file_name: str,
        file_directory: str = 'tmp',
        append: bool = False
):
    """
        A function for merging all tracklists from TMP directory.
        After merging, the result will be saved to a separate file
        inside the TMP directory.

        :param tmp_directory: name of folder with collected separate user tracklists
        :type tmp_directory: str

        :param file_name: If used with the "append" parameter, then
        the name of the file to which the merging result should be added is indicated.
        Otherwise it will be used as the name of the output file.
        :type file_name: str

        :param file_directory: Name of the folder containing the file to which
        you want to add the merging results
        :type file_directory: str

        :param append: Ability to add merge results to a specified file.
        :type append: bool
    """
    if append:
        # If there is already a file to which you want to add data
        # determine the path to it and load the data into the DataFrame:
        file_path = os.path.join(file_directory, f'{file_name}.csv')
        if os.path.exists(file_path):
            merged_df = pandas.read_csv(file_path)
        else:
            print(f"File wasn't found by this path: {file_path}")
            return
    else:
        # If there is no file to add, then create an empty DataFrame:
        merged_df = pandas.DataFrame()

    # Looping through all the files in the TMP directory:
    for file in os.listdir(tmp_directory):
        if file.endswith('_tracklist.csv'):
            file_path = os.path.join(tmp_directory, file)

            # Load the content of the CSV file into a DataFrame
            # and merge it with the common DataFrame:
            df = pandas.read_csv(file_path)
            merged_df = pandas.concat([merged_df, df], ignore_index=True)

    # Write the merged DataFrame to a CSV file:
    output_file_path = os.path.join(tmp_directory, f'{file_name}_result.csv')
    merged_df.to_csv(output_file_path, index=False)
