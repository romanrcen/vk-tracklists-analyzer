import vk_api
import file_handler

from vk_api import VkUserPermissions as scope, VkApi
from vk_api.audio import VkAudio
from vk_api.exceptions import AccessDenied

import helpers
from config import APP_ID
from constants import USERS_TRACKLIST_FILE_NAME


class VkClient(VkApi):
    def __init__(self, login=None, password=None, token=None):
        super().__init__(
            login=login,
            password=password,
            token=token,
            app_id=APP_ID,
            captcha_handler=self._captcha_handler,
            auth_handler=self._two_factor_auth_handler,
            scope=scope.FRIEND | scope.GROUPS | scope.OFFLINE | scope.AUDIO
        )

    @staticmethod
    def _captcha_handler(captcha):
        key = input(f'Введите код капчи:\n{captcha.get_url().strip()}')
        return captcha.try_again(key)

    @staticmethod
    def _two_factor_auth_handler():
        code = input("Введите код двухфакторной аутентификации: ")
        remember_device = False
        return str(code), remember_device

    def audio_module(self):
        return VkAudio(vk=self, convert_m3u8_links=False)

    def authorization(self, token_only=False):
        try:
            self.auth(token_only=token_only)
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return

        print('Authorization success')

    def get_group_members(self, group_id, count=False, extra_params=None):
        params = {'group_id': group_id}

        if extra_params:
            params.update(extra_params)

        response = self.method('groups.getMembers', params)

        if count:
            print(f"The group has {response.get('count')} members")
            return response.get('count')

        return response

    def get_group_members_info(self, group_id) -> dict:
        members_info = []

        # Determining the  group members count:
        members_count = self.get_group_members(group_id=group_id, count=True)

        # Collecting info:
        for request_number in range((members_count // 1000)):
            extra_params = {
                'sort': 'id_asc',
                'fields': 'city, sex',
                'offset': request_number * 1000
            }

            response = self.get_group_members(
                group_id=group_id,
                extra_params=extra_params
            )

            members_info += response.get('items')

        # Logging:
        if members_info:
            print('Members info successfully received')
        else:
            print('No members in group')

        return {
            item['id']: {
                'sex': item['sex'],
                'city': item['city']['title'] if 'city' in item else None
            } for item in members_info
        }

    def get_user_tracklist(self, user_id) -> list or AccessDenied:
        print(f'Collecting tracklist for user with id: {user_id}')

        try:
            tracklist = self.audio_module().get(owner_id=user_id)
        except AccessDenied as error:
            print('No access to tracklist')
            return error

        if tracklist:
            print(f'{len(tracklist)} tracks collected')
        else:
            print('Tracklist is empty')

        return tracklist

    def get_all_users_tracks(
            self,
            users_data: dict,
            start_index: int = None,
            end_index: int = None,
            export_file_name: str = USERS_TRACKLIST_FILE_NAME
    ):
        total = 0
        access_denied_count = 0
        empty_tracklist_count = 0

        # Make a cycle with progress bar:
        progress = helpers.progress_bar(list(users_data)[start_index:end_index])

        # Collecting user tracks:
        for user_id in progress:
            user_tracklist = self.get_user_tracklist(user_id)

            # Updating counts:
            if isinstance(user_tracklist, AccessDenied):
                access_denied_count += 1
            elif not user_tracklist:
                empty_tracklist_count += 1
            else:
                file_handler.export_tracklist_to_csv(
                    user_data=users_data.get(user_id),
                    tracklist=user_tracklist,
                    file_name=user_id
                )
                total += len(user_tracklist)

        file_handler.merge_csv_files(
            tmp_directory='tmp',
            file_name=export_file_name,
        )

        print(f'\n{total} tracks collected! Results inside the tmp/ directory')
        print(f'Users with private audio: {access_denied_count}')
        print(f'Users with empty tracklist: {empty_tracklist_count}')
