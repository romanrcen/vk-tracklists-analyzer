import pandas
import pprint


class Analyzer:
    def __init__(self, csv=None):
        self.csv = csv

    @property
    def data_frame(self):
        df = pandas.read_csv(self.csv)

        # Remove spaces before and after of texts
        df = df.map(lambda x: x.strip() if isinstance(x, str) else x)
        return df

    def get_top_of_tracks(self, chart_size: int = 10) -> dict:
        result = {}

        print('Building a tracks top chart...')
        # Filtering records from uninformative values:
        filtered_df = self.data_frame[
            (self.data_frame['artist'] != 'Неизвестен') &
            (self.data_frame['title'] != 'Без названия')
        ]

        # Group data by "artist" and "title" fields with unique owner id only:
        group = filtered_df.groupby(['artist', 'title'])['owner_id'].nunique().reset_index(name='count')

        # Sort groups by descending number of records:
        tracks_chart = group.sort_values(by='count', ascending=False).head(
            chart_size
        )

        # Build a top chart:
        for index, row in tracks_chart.iterrows():
            result[chart_size - len(tracks_chart) + 1] = {
                'track': f"{row['artist']} - {row['title']}",
                'value': f"{row['count']}"
            }
            chart_size += 1

        print(f'The most popular tracks:')
        pprint.pprint(result, indent=3, width=200)
        return result

    def get_top_of_artists(self, chart_size: int = 10) -> dict:
        result = {}

        print('Building an artists top chart...')
        # Filtering records from uninformative values:
        filtered_df = self.data_frame[
            (self.data_frame['artist'] != 'Неизвестен')
        ]

        # Group data by "artist" field with unique owner id only:
        group = filtered_df.groupby('artist')['owner_id'].nunique().reset_index(name='count')

        # Sort groups by descending number of records:
        artists_chart = group.sort_values(by='count', ascending=False).head(
            chart_size
        )

        # Build a top chart:
        for index, row in artists_chart.iterrows():
            result[chart_size - len(artists_chart) + 1] = {
                'artist': f"{row['artist']}",
                'value': f"{row['count']}"
            }
            chart_size += 1

        print(f'The most popular artists:')
        pprint.pprint(result, indent=3, width=200)
        return result
