import file_handler
from analyzer import Analyzer
from config import INDIA_GROUP_ID, PASSWORD, PHONE, TOKEN
from constants import USERS_TRACKLIST_FILE_NAME
from vk_client import VkClient

# VK client init:
vk_client = VkClient(login=PHONE, password=PASSWORD, token=TOKEN)

# Authorization:
vk_client.authorization(token_only=True)

# Collecting and export members info to CSV:
users_data = vk_client.get_group_members_info(INDIA_GROUP_ID)
file_handler.export_users_data_to_csv(users_data)

# Collecting users id from CSV file:
imported_users_data = file_handler.import_users_data_from_csv()

# Collecting and export users tracks to CSV:
vk_client.get_all_users_tracks(
    imported_users_data,
    start_index=6000,
    end_index=6500
)

# Determination of the most popular tracks and artists:
analyzer = Analyzer(f'{USERS_TRACKLIST_FILE_NAME}.csv')
analyzer.get_top_of_tracks(30)
analyzer.get_top_of_artists(30)

file_handler.merge_csv_files(
    tmp_directory='tmp',
    file_name=USERS_TRACKLIST_FILE_NAME,
    file_directory='',
    append=True
)
